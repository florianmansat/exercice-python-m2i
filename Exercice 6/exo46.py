
def digitize(n, base=10):
    if n == 0:
        yield 0
    while n:
        n, d = divmod(n, base)
        yield d


if __name__ == '__main__':
    print(list(reversed(tuple(digitize(int(input("n = ")))))))