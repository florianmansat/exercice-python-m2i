def Intersec(s1, s2):
    s1 = s1.split()
    s2 = s2.split()

    s1set = set(s1)

    intersection = s1set.intersection(s2)

    return list(intersection)


if __name__ == '__main__':
    print("les valeurs communes sont %s" % Intersec("sd dd ss", "dd ssd qsd"))