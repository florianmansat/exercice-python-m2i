
def nombreOccurences(l, x):
    n = 0

    for s in l:
        if x in s:
            n += 1
    return n


if __name__ == '__main__':
    print(nombreOccurences(['un', 'deux', 'deux'], 'deux'))