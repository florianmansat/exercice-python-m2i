
def countUpperAndLower(s):
    r = {'countUpper': 0, 'countLower': 0}

    for c in s:
        if c.islower():
            r['countLower'] += 1

        if c.isupper():
            r['countUpper'] += 1

    return r


if __name__ == '__main__':
    print(countUpperAndLower("Je suis CALME"))
