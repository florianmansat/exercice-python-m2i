from collections import Counter

if __name__ == '__main__':
    l = ["laptop", "iphone", "tablet"]

    for str in l:
        count = Counter(str)

        print("\nLa chaine %s de longueur %s possède les caractères suivants :" % (str, len(str)))
        for c in count:
            print("%s : %s" % (c, count[c]))
