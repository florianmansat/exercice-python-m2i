def powerset(seq):

    if len(seq) <= 1:
        yield seq
        yield []
    else:
        for item in powerset(seq[1:]):
            yield [seq[0]]+item
            yield item


if __name__ == '__main__':
    print([x for x in powerset([1, 2, 3])])