def decorateur(func):
    def compter(*args, **argv):
        compter.calls += 1
        return func(*args, **argv)

    compter.calls = 0
    return compter


@decorateur
def decorer(x):
    return x + 1


if __name__ == '__main__':
    print(decorer(0))
    print(decorer(1))
    print("number of calls:", decorer.calls)
