
if __name__ == '__main__':
    n = int(input("n = "))

    div = []

    for i in range(1, n + 1):
        if n % i == 0:
            div.append(i)

    print("les diviseurs de %s sont %s" % (n, div))