if __name__ == '__main__':
    n = int(input("a = "))
    i = 2

    while i < n and n % i != 0:
        i = i + 1

    if i == n:
        print("%s est un nombre premier" % n)
    else :
        print("%s n'est pas un nombre premier" % n)
