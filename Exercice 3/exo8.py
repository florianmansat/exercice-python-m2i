
if __name__ == '__main__':
    n = int(input("n = "))
    somme = 0

    for i in range(1, n + 1):
        print("%s += %s" % (somme, i))
        somme += i


    print("Somme de 1 à n = %s" % somme)