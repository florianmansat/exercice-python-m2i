
def est_carre_parfait(n):
    x = n // 2
    array = {x}
    while x * x != n:
        x = (x + (n // x)) // 2
        if x in array:
            return False
        array.add(x)
    return True


if __name__ == '__main__':
    n = int(input("n = "))

    if est_carre_parfait(n):
        print("%s est un carré parfait" % n)
    else:
        print("%s n'est pas un carré parfait" % n)


