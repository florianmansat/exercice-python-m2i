from math import *

if __name__ == '__main__':
    r = int(input("rayon du cercle = "))
    s = (r ^ 2) * pi
    p = r * 2 * pi

    print("Surface = %s \nPerimetre = %s" % (s, p))
