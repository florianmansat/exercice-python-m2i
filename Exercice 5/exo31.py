
if __name__ == '__main__':
    l = [0, 1, 2, 3]
    pai = []
    imp = []

    for i in l:
        if i % 2 == 0:
            pai.append(i)
        else:
            imp.append(i)

    print("liste des pairs : %s" % pai)
    print("liste des impairs : %s" % imp)